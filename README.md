# springBoot-console
* 概述：
    * 前端html（前后端分离，利用ajax交互）
    * 技术：BootStrap + layer + jquery + css + html

1、前端模板：https://gitee.com/layuicms/layuicms

2、代码托管：https://gitee.com/enzoism/springboot-layui-console2

3、使用参考：
- [本DEMO]其他页面可以参考user_manager进行复制，然后修改
- layui选中select-option：https://www.cnblogs.com/niuben/p/11127544.html
- layui关于form表单模块之select带搜索功能：https://blog.csdn.net/qq_28137309/article/details/82830453
- layui关于判断checkbox 是否选中：https://blog.csdn.net/InnovationAD/article/details/80495044

4、本地切换远程
localhost:8099  http://47.114.76.95:8099

5、远程部署
1）下载代码
git clone https://gitee.com/enzoism/SpringBoot-Seed.git -b car-manager

2）安装mvn
yum install -y maven

3）mvn打包
mvn clean package

4）进行打包
nohup java -jar car-Manager-0.0.1-SNAPSHOT.jar > log_car_manager.file 2>&1 &

5）静态文件直接挂载（纯JS，没有jsp页面）
docker run -d -p 8083:8080 -v /apps/project/web-server/springboot-layui-console2:/usr/local/tomcat/webapps/ROOT registry.cn-hangzhou.aliyuncs.com/acs-sample/tomcat
