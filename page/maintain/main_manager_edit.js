var $;
var requestURL = 'http://139.9.229.30:8099/';
layui.config({
	base : "js/"
}).use(['form','layer','jquery'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		laypage = layui.laypage;
		$ = layui.jquery;

    // 进行初始数据请求
    requestData();
    // 监听按钮提交
    form.on("submit(updateUser)",function(data){
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: requestURL+"maintain/hist/update",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                withCredentials: true,
                crossDomain: true,
                data: JSON.stringify(data.field),
                success: function (data) {
                    top.layer.close(index);
                    top.layer.msg(JSON.stringify(data.data));
                    layer.closeAll("iframe");
                    //刷新父页面
                    parent.location.reload();
                }
            })
            layer.close(index);
        }, 1000);
        return false;
    })
    console.log("success");
})

// 请求数据
function requestData(){
    $.ajax({
        type: "POST",
        url: requestURL+"/maintain/hist/detail?id="+getQueryVariable("id"),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        withCredentials: true,
        crossDomain: true,
        success: function (data) {
            if(data && data.code == 200) {
                var data = data.data;
                $('#id').val(data.id);
                $('#nickName').val(data.nickName);
                $('#userPhone').val(data.userPhone);
                $('#carNumber').val(data.carNumber);
                $('#mainCost').val(data.mainCost);
                $('#mainDesc').val(data.mainDesc);

                // 判断checkbox 是否选中
                $('#mainPay').attr("checked", true); //注意这里使用的是attr()

                // form刷新
                layui.form().render();
            }
        }
    })
}
//格式化时间
function formatTime(_time){
    var year = _time.getFullYear();
    var month = _time.getMonth()+1<10 ? "0"+(_time.getMonth()+1) : _time.getMonth()+1;
    var day = _time.getDate()<10 ? "0"+_time.getDate() : _time.getDate();
    var hour = _time.getHours()<10 ? "0"+_time.getHours() : _time.getHours();
    var minute = _time.getMinutes()<10 ? "0"+_time.getMinutes() : _time.getMinutes();
    return year+"-"+month+"-"+day+" "+hour+":"+minute;
}
function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}